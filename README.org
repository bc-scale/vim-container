#+TITLE: Vim Container
A nix flake that creates a docker image with a simple vim configuration in
it.
This is intended to be a base image that other project images are built off
so they can have access to a common vim.
