{
  description = "Simple vim config in a container";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        vim = (pkgs.neovim.override {
          viAlias = true;
          configure = {
            packages.myPlugins = with pkgs.vimPlugins; {
              start = [
                Supertab
                fugitive
                LanguageClient-neovim
                vim-gitgutter
                vim-slime
                (nvim-treesitter.withPlugins (plugins: [
                  plugins.tree-sitter-python
                  plugins.tree-sitter-c
                ]))
              ];
            };
            customRC = ''
              set nocompatible

              filetype plugin indent on

              " General Vim
              set updatetime=100
              set nobackup
              set noswapfile

              " Colors and fonts
              syntax enable
              syntax spell toplevel
              colorscheme morning
              set termguicolors

              " ui config
              set	mouse=a
              set number relativenumber
              hi! link FoldColumn Normal
              set foldcolumn=3
              set lazyredraw

              " Tab complete
              set wildmode=longest,list,full
              set wildmenu
              set wildignore=*.o,*~,*.pyc,*/.git/*,*/.hg/*,*/.svn/*,.gitignore

              " Show invisibles
              set list
              set listchars=tab:▸\ ,eol:¬

              " Searching and autocomplete
              set ignorecase
              set smartcase
              set showmatch
              set incsearch
              set hlsearch
              set complete=.

              " Tabs and spaces
              set tabstop=4
              set softtabstop=4
              set shiftwidth=4
              set autoindent
              set cc=80
              set backspace=indent,eol,start
              set scrolloff=7

              " Comment wrapping
              set textwidth=79
              set formatoptions=cqr

              " Leader shortcuts
              nnoremap <space> <Nop>
              let mapleader=" "
              let localmapleader=","

              " Save and quit
              nnoremap <leader>w :w<CR>
              nnoremap <leader>q :q<CR>
              nnoremap <leader>wq :wq<CR>
              nnoremap <leader>Q :qa<CR>
                "Don't enter ex mode
              noremap Q <nop>


              " Command-line mode
              nnoremap <leader>; :<up><CR>
              cnoremap <C-a> <home>
              cnoremap <C-f> <right>
              cnoremap <C-b> <left>
              cnoremap <C-n> <down>
              cnoremap <C-p> <up>

              " Comments
              nnoremap <silent> <leader>c :set opfunc=comments#CommentLines<CR>g@
              nnoremap <silent> <leader>cl :call comments#CommentLines(v:count1)<CR>
              nnoremap <silent> gcc :call comments#CommentLines(v:count1)<CR>

              	" Change surrounding parenthesis/brackets.
              nnoremap <leader>( mt%r)`tr(
              nnoremap <leader>{ mt%r}`tr{
              nnoremap <leader>[ mt%r]`tr[

              nnoremap <leader>) mt%r(`tr)
              nnoremap <leader>} mt%r{`tr}
              nnoremap <leader>] mt%r[`tr]

              	" Increment and deincrement
              nnoremap <leader>+ <C-a>
              nnoremap <leader>- <C-x>

              	" Toggles
              nnoremap <leader>S :set spell!<CR>
              nnoremap <leader>/ :nohl<CR>

              " Remap
              nnoremap Y y$
              nnoremap j gj
              nnoremap k gk
              nnoremap J <C-e>
              nnoremap K <C-y>
              noremap L $
              noremap H ^
              nnoremap <leader>? K

              " Auto close
              inoremap " ""<left>
              inoremap ' '\'<left>
              inoremap ( ()<left>
              inoremap [ []<left>
              inoremap { {}<left>
              inoremap (<CR> (<CR>)<ESC>O
              inoremap {<CR> {<CR>}<ESC>O
              inoremap {;<CR> {<CR>};<ESC>O

              " Autocomplete
              inoremap C-f C-xc-f
              imap <C-space> <tab>
              cmap <C-space> <tab>

              " Plugin Options
              	" SuperTab
              let g:SuperTabDefaultCompletionType="context"
              let g:SuperTabCompletionContexts=['s:ContextText', 's:ContextDiscover']
              let g:SuperTabContextTextOmniPrecedence=['&completefunc', '&omnifunc']
              let g:SuperTabContextDiscoverDiscovery=
              		\ ["&completefunc:<c-p>", "&omnifunc:<c-x><c-o>"]

              	" Git gutter
              let g:gitgutter_map_keys=0

                " Slime
              let g:slime_target = "tmux"
              let g:slime_default_config = {"socket_name": "default", "target_pane": "{last}"}

                " Language Server
              let g:LanguageClient_serverCommands = {
                  \ 'python': ['pyls']
                  \ }
              nnoremap <F5> :call LanguageClient_contextMenu()<CR>
              nnoremap <silent> gh :call LanguageClient_textDocument_hover()<CR>
              nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
              nnoremap <silent> gr :call LanguageClient_textDocument_references()<CR>
              nnoremap <silent> gq :call LanguageClient_textDocument_formatting()<CR>,start
            '';
          };
        });
        vimDockerImage = pkgs.dockerTools.buildImage {
          name = "vim-config";
          tag = "latest";
          config = { Env = [ "HOME=/home/docker" "EDITOR=vim" ]; };
          runAsRoot = ''
            mkdir /tmp
          '';
          contents = [
            vim
            (pkgs.buildEnv {
              name = "image-root";
              pathsToLink = [ "/bin" ];
              paths = [ pkgs.git pkgs.tmux ];
            })
          ];
        };
      in {
        packages.dockerImage = vimDockerImage;
        packages.default = self.packages.${system}.dockerImage;
      });
}
